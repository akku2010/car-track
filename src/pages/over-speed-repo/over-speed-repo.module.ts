import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverSpeedRepoPage } from './over-speed-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    OverSpeedRepoPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(OverSpeedRepoPage),
    SelectSearchableModule,
    IonicSelectableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class OverSpeedRepoPageModule {}
